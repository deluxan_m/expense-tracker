import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {colors} from '../../config/colors';

interface Savings {
  id: number;
  icon: string;
  name: string;
  amount: number;
  target: number;
  current: number;
}

interface IProps {
  savings: Array<Savings>;
}

const Savings = (props: IProps) => {
  const {savings} = props;

  return (
    <View style={{marginTop: 20}}>
      <Text style={styles.title}>Savings</Text>
      {savings.map((saving: Savings) => {
        return (
          <View key={saving.id} style={styles.container}>
            <Icon
              name={saving.icon}
              color="#000"
              size={30}
              style={styles.icon}
            />
            <View style={styles.textContainer}>
              <Text style={styles.name}>{saving.name}</Text>
              <Text style={styles.amount}>{saving.amount} LKR</Text>
            </View>
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginVertical: 5,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginVertical: 10,
  },
  icon: {
    backgroundColor: colors.P_LIGHT,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 10,
  },
  textContainer: {
    marginLeft: 20,
  },
  name: {
    fontSize: 16,
  },
  amount: {
    fontSize: 12,
  },
  star: {
    paddingVertical: 5,
    marginLeft: 20,
  },
});

export default Savings;
