import React from 'react';
import {ScrollView} from 'react-native';

import Header from './Header';
import List from './List';
import Savings from './Savings';

const accounts = [
  {
    id: 1,
    icon: 'home',
    name: 'Wallet',
    amount: 500,
    isPrimary: true,
  },
  {
    id: 2,
    icon: 'home',
    name: 'Commercial Bank',
    amount: 1500,
    isPrimary: false,
  },
  {
    id: 3,
    icon: 'home',
    name: 'Peoples Bank',
    amount: 5000,
    isPrimary: false,
  },
];

const savings = [
  {
    id: 1,
    icon: 'home',
    name: 'My Savings 1',
    amount: 500,
    target: 100000,
    current: 10000,
  },
  {
    id: 2,
    icon: 'home',
    name: 'My Savings 2',
    amount: 1500,
    isPrimary: false,
    target: 80000,
    current: 10000,
  },
  {
    id: 3,
    icon: 'home',
    name: 'My Savings 3',
    amount: 5000,
    isPrimary: false,
    target: 5000,
    current: 1200,
  },
];

const Accounts = () => {
  return (
    <ScrollView>
      <Header />
      <List accounts={accounts} />
      <Savings savings={savings} />
    </ScrollView>
  );
};

export default Accounts;
