import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FontistoIcon from 'react-native-vector-icons/Fontisto';

import Accounts from '../screens/Accounts';
import Categories from '../screens/Categories';
import Transactions from '../screens/Transactions';
import Budget from '../screens/Budget';
import Dashboard from '../screens/Dashboard';
import {colors} from '../config/colors';

const Tab = createBottomTabNavigator();

export default function BottomNavigation() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          if (route.name === 'Accounts') {
            return (
              <View style={styles.tabMenu}>
                <EntypoIcon name="wallet" color={color} size={size} />
                <Text style={styles.tabMenuName}>Accounts</Text>
              </View>
            );
          } else if (route.name === 'Categories') {
            return (
              <View style={styles.tabMenu}>
                <MaterialIcon name="category" color={color} size={size} />
                <Text style={styles.tabMenuName}>Category</Text>
              </View>
            );
          } else if (route.name === 'Transactions') {
            return (
              <View style={[styles.tabMenu, styles.centerMenu]}>
                <FontistoIcon name="arrow-swap" color={color} size={size} />
                <Text style={styles.tabMenuName}>Manage</Text>
              </View>
            );
          } else if (route.name === 'Budget') {
            return (
              <View style={styles.tabMenu}>
                <FAIcon name="area-chart" color={color} size={size} />
                <Text style={styles.tabMenuName}>Budget</Text>
              </View>
            );
          } else if (route.name === 'Dashboard') {
            return (
              <View style={styles.tabMenu}>
                <FAIcon name="pie-chart" color={color} size={size} />
                <Text style={styles.tabMenuName}>Dashboard</Text>
              </View>
            );
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: colors.PRIMARY,
        showLabel: false,
        style: {
          position: 'absolute',
          bottom: 25,
          left: 20,
          right: 20,
          borderRadius: 15,
          height: 90,
          ...styles.shadow,
        },
      }}>
      <Tab.Screen name="Accounts" component={Accounts} />
      <Tab.Screen name="Categories" component={Categories} />
      <Tab.Screen name="Transactions" component={Transactions} />
      <Tab.Screen name="Budget" component={Budget} />
      <Tab.Screen name="Dashboard" component={Dashboard} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#7F5DF0',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  tabMenu: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerMenu: {
    backgroundColor: colors.SECONDARY,
    borderRadius: 15,
    padding: 15,
    bottom: 40,
  },
  tabMenuName: {
    fontSize: 10,
  },
});
