import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {colors} from '../../config/colors';

const Header = () => {
  return (
    <View style={styles.container}>
      <View style={styles.nameContainer}>
        <Text style={styles.name}>All Accounts</Text>
        <Icon name="caret-down" color="#fff" size={14} />
      </View>
      <View style={styles.nameContainer}>
        <Text style={[styles.name, styles.currency]}>LKR</Text>
        <Text style={styles.amount}>191,135</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.P_DARK,
    alignItems: 'center',
    height: 120,
  },
  nameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  name: {
    color: colors.P_TEXT,
    marginRight: 5,
  },
  currency: {
    fontSize: 10,
  },
  amount: {
    color: colors.P_TEXT,
  },
});

export default Header;
