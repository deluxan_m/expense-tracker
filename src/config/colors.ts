export const colors = {
  PRIMARY: '#1976d2',
  P_LIGHT: '#63a4ff',
  P_DARK: '#004ba0',
  P_TEXT: '#ffffff',

  SECONDARY: '#80d8ff',
  S_LIGHT: '#b5ffff',
  S_DARK: '#49a7cc',
  S_TEXT: '#000000',
};
