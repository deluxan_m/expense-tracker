import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {colors} from '../../config/colors';

interface Account {
  id: number;
  icon: string;
  name: string;
  amount: number;
  isPrimary: boolean;
}

interface IProps {
  accounts: Array<Account>;
}

const AccountsList = (props: IProps) => {
  const {accounts} = props;

  return (
    <View>
      <Text style={styles.title}>Accounts</Text>
      {accounts.map((account: Account) => {
        return (
          <View key={account.id} style={styles.container}>
            <Icon
              name={account.icon}
              color="#000"
              size={30}
              style={styles.icon}
            />
            <View style={styles.textContainer}>
              <Text style={styles.name}>{account.name}</Text>
              <Text style={styles.amount}>{account.amount} LKR</Text>
            </View>
            <Icon
              name={account.isPrimary ? 'star' : 'star-o'}
              color="#000"
              size={18}
              style={styles.star}
            />
          </View>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginVertical: 5,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 20,
    marginVertical: 10,
  },
  icon: {
    backgroundColor: colors.P_LIGHT,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 10,
  },
  textContainer: {
    marginLeft: 20,
  },
  name: {
    fontSize: 16,
  },
  amount: {
    fontSize: 12,
  },
  star: {
    paddingVertical: 5,
    marginLeft: 20,
  },
});

export default AccountsList;
